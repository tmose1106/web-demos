let calendar_object = new Calendar();

let selection_span = document.getElementById("selection-name");
calendar_object.onSelection = function() {
  let current_selection = calendar_object.selectedDate;
  if (current_selection)
    selection_span.textContent = current_selection.toLocaleDateString();
  else
    selection_span.textContent = "None";
}

let month_back = document.getElementById("month-back");
month_back.onclick = function() {
  calendar_object.decrementMonth();
  calendar_object.reset();
}

let month_next = document.getElementById("month-next");
month_next.onclick = function() {
  calendar_object.incrementMonth();
  calendar_object.reset();
}

/* The class for handling calendar functions. */

function _days_in_month(month_index, year_int)
{
  var is_leap_year = ((year_int % 4) ? false : true);
  switch (month_index) {
    case 0:
    case 2:
    case 4:
    case 6:
    case 7:
    case 9:
    case 11:
      return 31;
    case 3:
    case 5:
    case 8:
    case 10:
      return 30;
    case 1:
      return (is_leap_year ? 29 : 28);
    default:
      return;
  }
}

function _month_start_day(month_index, year_int)
/*
  Compute the first day of the month using only the month (as 0 - 11) and
  the year. This function impliments disparate variation of Gauss's
  algorithm as defined on the Wikipedia page for "Determination of the day
  of the week".

  It returns a number from 0 - 6, assuming the start of the week (0) is
  Sunday.
*/
{
  //  We shift the month so that the year begins with March and ends with
  // February
  var shift_month = ((month_index > 1) ? (month_index - 1) : (month_index + 11));

  var shift_year = year_int;
  var year_string = String(shift_year);
  var year_start = Number(year_string.slice(0,2));
  var year_end = Number(year_string.slice(2));

  if (month_index < 2) {
    year_end -= 1
  }

  var result = (1 + Math.floor((2.6 * shift_month - 0.2)) + year_end + Math.floor((year_end / 4)) + Math.floor((year_start / 4)) - (2 * year_start));

  while (result < 0) {
    result += 7;
  }

  return (result % 7);
}

class Calendar {
  constructor()
  /* initial_date is the date object we would like to start at */
  {
    this._months = ["January", "February", "March", "April", "May", "June",
                   "July", "August", "September", "October", "November",
                   "December"];
    this._days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday",
                "Friday", "Saturday"];

    this._calendar_div = document.getElementById("calendar");
    this._date_div = document.getElementById("days-container");
    this._month_span = document.getElementById("month-label");

    let current_date = new Date();
    this._display_year = this.current_year = current_date.getFullYear();
    this._display_month = this.current_month = current_date.getMonth();
    this.current_day = current_date.getDate();

    this._selected_year = this._selected_month = this._selected_day = undefined;

    this._label_month();
    this._add_dates();
    this._style_dates();
  }

  _add_dates()
  {
    let starting_index = _month_start_day(this._display_month, this._display_year);

    if (starting_index) {
      let leading_spacer = document.createElement("span");
      leading_spacer.classList.add("day-spacer");
      leading_spacer.style.gridColumnEnd = starting_index + 1;
      this._date_div.appendChild(leading_spacer);
    }

    let days_in_month = _days_in_month(this._display_month, this._display_year);

    // For each date in the month...
    for (let index = 1; index <= days_in_month; index++) {
      let day_box = document.createElement("span");
      day_box.classList.add("day-box");
      day_box.textContent = index;
      // Refer the objects self for use in onclick below
      let self = this;
      day_box.onclick = function()
      {
        self._selected_year = self._display_year;
        self._selected_month = self._display_month;
        self._selected_day = Number(this.textContent);
        self._style_dates();
        self.onSelection();
      }
      this._date_div.appendChild(day_box);
    }
  }

  _clear_dates() {
    while(this._date_div.firstChild)
      this._date_div.removeChild(this._date_div.firstChild);
  }

  _label_month() { this._month_span.textContent = this._months[this._display_month] + " " + this._display_year; }

  _style_dates()
  {
    let date_start_offset;
    if (this._date_div.children[0].classList.contains("day-spacer"))
      date_start_offset = 0;
    else
      date_start_offset = 1;

    for (let i = 1, len = this._date_div.children.length; i < len; i++) {
      let date_box = this._date_div.children[i - date_start_offset];
      if ((i == this.current_day) && ((this._display_year == this.current_year) && (this._display_month == this.current_month)))
        date_box.classList.add("current-date");
      else
        date_box.classList.remove("current-date");

      if ((this._display_year == this._selected_year) && (this._display_month == this._selected_month) && (i == this._selected_day))
        date_box.classList.add("selected-date");
      else
        date_box.classList.remove("selected-date");
    }
  }

  decrementMonth()
  {
    if (this._display_month > 0)
      this._display_month -= 1;
    else if (this._display_month == 0) {
      this._display_year -= 1;
      this._display_month = 11;
    }
  }

  decrementYear()
  {
    this._display_year -= 1;
  }

  incrementMonth()
  {
    if (this._display_month < 11)
      this._display_month += 1;
    else if (this._display_month == 11) {
      this._display_year += 1;
      this._display_month = 0;
    }
  }

  incrementYear()
  {
    this._display_year += 1;
  }

  reset()
  {
    this._label_month();
    this._clear_dates();
    this._add_dates();
    this._style_dates();
  }

  get displayedMonth() { return new Date(this._display_year, this._display_month); }

  get selectedDate() {
    if ((this._selected_day) && ((this._selected_month >= 0) && (this._selected_month < 12)) && (this._selected_year))
      return new Date(this._selected_year, this._selected_month, this._selected_day);
    else
      return;
  }

  onSelection() {}
}
